﻿namespace Practica2_VB_Pizzeria
{
    partial class FormPizzeria
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPizzeria));
            this.labelCartel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelSeleccionPizza = new System.Windows.Forms.Label();
            this.labelListaIngredientes = new System.Windows.Forms.Label();
            this.labelMasaPizza = new System.Windows.Forms.Label();
            this.labelTamañoPizza = new System.Windows.Forms.Label();
            this.comboBoxMenuPizza = new System.Windows.Forms.ComboBox();
            this.comboBoxMasa = new System.Windows.Forms.ComboBox();
            this.comboBoxTamaño = new System.Windows.Forms.ComboBox();
            this.checkedListBoxIngredientes = new System.Windows.Forms.CheckedListBox();
            this.buttonPedirPizza = new System.Windows.Forms.Button();
            this.richTextBoxResumenDeCompra = new System.Windows.Forms.RichTextBox();
            this.labelListaResumenDeCompra = new System.Windows.Forms.Label();
            this.labelIngresientesPorDefecto = new System.Windows.Forms.Label();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.buttonImporteFinal = new System.Windows.Forms.Button();
            this.checkedListBoxIngredientesDefecto = new System.Windows.Forms.CheckedListBox();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCartel
            // 
            this.labelCartel.AutoSize = true;
            this.labelCartel.BackColor = System.Drawing.Color.White;
            this.labelCartel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCartel.Location = new System.Drawing.Point(516, 29);
            this.labelCartel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCartel.Name = "labelCartel";
            this.labelCartel.Size = new System.Drawing.Size(206, 39);
            this.labelCartel.TabIndex = 0;
            this.labelCartel.Text = "Bienvenidos";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(425, 20);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(425, 48);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.Location = new System.Drawing.Point(804, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(420, 48);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pictureBox3.Location = new System.Drawing.Point(25, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(425, 48);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // labelSeleccionPizza
            // 
            this.labelSeleccionPizza.AutoSize = true;
            this.labelSeleccionPizza.BackColor = System.Drawing.Color.Transparent;
            this.labelSeleccionPizza.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSeleccionPizza.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelSeleccionPizza.Location = new System.Drawing.Point(21, 75);
            this.labelSeleccionPizza.Name = "labelSeleccionPizza";
            this.labelSeleccionPizza.Size = new System.Drawing.Size(181, 24);
            this.labelSeleccionPizza.TabIndex = 4;
            this.labelSeleccionPizza.Text = "Seleccione su pizza";
            // 
            // labelListaIngredientes
            // 
            this.labelListaIngredientes.AutoSize = true;
            this.labelListaIngredientes.BackColor = System.Drawing.Color.Transparent;
            this.labelListaIngredientes.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelListaIngredientes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelListaIngredientes.Location = new System.Drawing.Point(21, 138);
            this.labelListaIngredientes.Name = "labelListaIngredientes";
            this.labelListaIngredientes.Size = new System.Drawing.Size(109, 24);
            this.labelListaIngredientes.TabIndex = 5;
            this.labelListaIngredientes.Text = "Ingredientes";
            // 
            // labelMasaPizza
            // 
            this.labelMasaPizza.AutoSize = true;
            this.labelMasaPizza.BackColor = System.Drawing.Color.Transparent;
            this.labelMasaPizza.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMasaPizza.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelMasaPizza.Location = new System.Drawing.Point(43, 438);
            this.labelMasaPizza.Name = "labelMasaPizza";
            this.labelMasaPizza.Size = new System.Drawing.Size(54, 24);
            this.labelMasaPizza.TabIndex = 6;
            this.labelMasaPizza.Text = "Masa";
            // 
            // labelTamañoPizza
            // 
            this.labelTamañoPizza.AutoSize = true;
            this.labelTamañoPizza.BackColor = System.Drawing.Color.Transparent;
            this.labelTamañoPizza.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTamañoPizza.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelTamañoPizza.Location = new System.Drawing.Point(21, 468);
            this.labelTamañoPizza.Name = "labelTamañoPizza";
            this.labelTamañoPizza.Size = new System.Drawing.Size(76, 24);
            this.labelTamañoPizza.TabIndex = 7;
            this.labelTamañoPizza.Text = "Tamaño";
            // 
            // comboBoxMenuPizza
            // 
            this.comboBoxMenuPizza.BackColor = System.Drawing.SystemColors.ControlLight;
            this.comboBoxMenuPizza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMenuPizza.FormattingEnabled = true;
            this.comboBoxMenuPizza.Items.AddRange(new object[] {
            "Selecciona una pizza ...",
            "NewYork (3,5€)",
            "Vegetariana (2€)",
            "Barbacoa Picante (4,5€)",
            "Cuatro Quesos (3€)"});
            this.comboBoxMenuPizza.Location = new System.Drawing.Point(25, 111);
            this.comboBoxMenuPizza.Name = "comboBoxMenuPizza";
            this.comboBoxMenuPizza.Size = new System.Drawing.Size(189, 24);
            this.comboBoxMenuPizza.TabIndex = 8;
            this.comboBoxMenuPizza.SelectedIndexChanged += new System.EventHandler(this.comboBoxMenuPizza_SelectedIndexChanged);
            // 
            // comboBoxMasa
            // 
            this.comboBoxMasa.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.comboBoxMasa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMasa.FormattingEnabled = true;
            this.comboBoxMasa.Items.AddRange(new object[] {
            "Selecciona un masa ...",
            "Fina (0,5€)",
            "Pan (1€)",
            "Tradicional (1,5€)",
            "Bordes Rellenos (3€)"});
            this.comboBoxMasa.Location = new System.Drawing.Point(103, 438);
            this.comboBoxMasa.Name = "comboBoxMasa";
            this.comboBoxMasa.Size = new System.Drawing.Size(189, 24);
            this.comboBoxMasa.TabIndex = 9;
            // 
            // comboBoxTamaño
            // 
            this.comboBoxTamaño.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.comboBoxTamaño.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTamaño.FormattingEnabled = true;
            this.comboBoxTamaño.Items.AddRange(new object[] {
            "Selecciona un tamaño ...",
            "Pequeño (3€)",
            "Mediano (4,5€)",
            "Familiar (6,5€)"});
            this.comboBoxTamaño.Location = new System.Drawing.Point(103, 468);
            this.comboBoxTamaño.Name = "comboBoxTamaño";
            this.comboBoxTamaño.Size = new System.Drawing.Size(189, 24);
            this.comboBoxTamaño.TabIndex = 10;
            // 
            // checkedListBoxIngredientes
            // 
            this.checkedListBoxIngredientes.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkedListBoxIngredientes.FormattingEnabled = true;
            this.checkedListBoxIngredientes.Items.AddRange(new object[] {
            "Jamón",
            "Champi",
            "Calabacín",
            "Aceitunas",
            "Tabasco",
            "Ternera",
            "Queso Mozzarellaa",
            "Queso Azul",
            "Queso Parmesano",
            "Queso Provolone",
            "Piña",
            "Peperoni",
            "Salami",
            "Cebolla",
            "Tomate",
            "Albahaca",
            "Pimienta",
            "Setas"});
            this.checkedListBoxIngredientes.Location = new System.Drawing.Point(25, 168);
            this.checkedListBoxIngredientes.Name = "checkedListBoxIngredientes";
            this.checkedListBoxIngredientes.Size = new System.Drawing.Size(270, 259);
            this.checkedListBoxIngredientes.TabIndex = 11;
            // 
            // buttonPedirPizza
            // 
            this.buttonPedirPizza.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonPedirPizza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPedirPizza.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPedirPizza.ForeColor = System.Drawing.Color.Maroon;
            this.buttonPedirPizza.Location = new System.Drawing.Point(25, 507);
            this.buttonPedirPizza.Name = "buttonPedirPizza";
            this.buttonPedirPizza.Size = new System.Drawing.Size(117, 40);
            this.buttonPedirPizza.TabIndex = 12;
            this.buttonPedirPizza.Text = "Pedir Pizza";
            this.buttonPedirPizza.UseVisualStyleBackColor = false;
            this.buttonPedirPizza.Click += new System.EventHandler(this.buttonPedirPizza_Click);
            // 
            // richTextBoxResumenDeCompra
            // 
            this.richTextBoxResumenDeCompra.Location = new System.Drawing.Point(667, 173);
            this.richTextBoxResumenDeCompra.Name = "richTextBoxResumenDeCompra";
            this.richTextBoxResumenDeCompra.Size = new System.Drawing.Size(286, 360);
            this.richTextBoxResumenDeCompra.TabIndex = 13;
            this.richTextBoxResumenDeCompra.Text = "";
            // 
            // labelListaResumenDeCompra
            // 
            this.labelListaResumenDeCompra.AutoSize = true;
            this.labelListaResumenDeCompra.BackColor = System.Drawing.Color.Transparent;
            this.labelListaResumenDeCompra.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F);
            this.labelListaResumenDeCompra.ForeColor = System.Drawing.Color.White;
            this.labelListaResumenDeCompra.Location = new System.Drawing.Point(664, 146);
            this.labelListaResumenDeCompra.Name = "labelListaResumenDeCompra";
            this.labelListaResumenDeCompra.Size = new System.Drawing.Size(209, 24);
            this.labelListaResumenDeCompra.TabIndex = 14;
            this.labelListaResumenDeCompra.Text = "Resumen de tu pedido";
            // 
            // labelIngresientesPorDefecto
            // 
            this.labelIngresientesPorDefecto.AutoSize = true;
            this.labelIngresientesPorDefecto.BackColor = System.Drawing.Color.Transparent;
            this.labelIngresientesPorDefecto.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F);
            this.labelIngresientesPorDefecto.ForeColor = System.Drawing.Color.White;
            this.labelIngresientesPorDefecto.Location = new System.Drawing.Point(313, 138);
            this.labelIngresientesPorDefecto.Name = "labelIngresientesPorDefecto";
            this.labelIngresientesPorDefecto.Size = new System.Drawing.Size(233, 24);
            this.labelIngresientesPorDefecto.TabIndex = 15;
            this.labelIngresientesPorDefecto.Text = "Ingredientes por defecto: ";
            // 
            // buttonVolver
            // 
            this.buttonVolver.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonVolver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVolver.ForeColor = System.Drawing.Color.Maroon;
            this.buttonVolver.Location = new System.Drawing.Point(25, 553);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(117, 40);
            this.buttonVolver.TabIndex = 16;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = false;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // buttonImporteFinal
            // 
            this.buttonImporteFinal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonImporteFinal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonImporteFinal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImporteFinal.ForeColor = System.Drawing.Color.Maroon;
            this.buttonImporteFinal.Location = new System.Drawing.Point(148, 553);
            this.buttonImporteFinal.Name = "buttonImporteFinal";
            this.buttonImporteFinal.Size = new System.Drawing.Size(117, 40);
            this.buttonImporteFinal.TabIndex = 17;
            this.buttonImporteFinal.Text = "Importe";
            this.buttonImporteFinal.UseVisualStyleBackColor = false;
            this.buttonImporteFinal.Click += new System.EventHandler(this.buttonImporte_Click);
            // 
            // checkedListBoxIngredientesDefecto
            // 
            this.checkedListBoxIngredientesDefecto.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkedListBoxIngredientesDefecto.Enabled = false;
            this.checkedListBoxIngredientesDefecto.FormattingEnabled = true;
            this.checkedListBoxIngredientesDefecto.Location = new System.Drawing.Point(318, 173);
            this.checkedListBoxIngredientesDefecto.Name = "checkedListBoxIngredientesDefecto";
            this.checkedListBoxIngredientesDefecto.Size = new System.Drawing.Size(306, 157);
            this.checkedListBoxIngredientesDefecto.TabIndex = 18;
            // 
            // buttonSalir
            // 
            this.buttonSalir.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSalir.ForeColor = System.Drawing.Color.Maroon;
            this.buttonSalir.Location = new System.Drawing.Point(1118, 553);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(117, 40);
            this.buttonSalir.TabIndex = 19;
            this.buttonSalir.Text = "Salir";
            this.buttonSalir.UseVisualStyleBackColor = false;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.BackColor = System.Drawing.Color.Transparent;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Yi Baiti", 18F);
            this.labelInfo.ForeColor = System.Drawing.Color.White;
            this.labelInfo.Location = new System.Drawing.Point(314, 333);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(327, 48);
            this.labelInfo.TabIndex = 20;
            this.labelInfo.Text = "No se pueden agregar ingredientes \nextra a esta pizza";
            this.labelInfo.Visible = false;
            // 
            // FormPizzeria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Practica2_VB_Pizzeria.Properties.Resources.back;
            this.ClientSize = new System.Drawing.Size(1247, 630);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.checkedListBoxIngredientesDefecto);
            this.Controls.Add(this.buttonImporteFinal);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.labelIngresientesPorDefecto);
            this.Controls.Add(this.labelListaResumenDeCompra);
            this.Controls.Add(this.richTextBoxResumenDeCompra);
            this.Controls.Add(this.buttonPedirPizza);
            this.Controls.Add(this.checkedListBoxIngredientes);
            this.Controls.Add(this.comboBoxTamaño);
            this.Controls.Add(this.comboBoxMasa);
            this.Controls.Add(this.comboBoxMenuPizza);
            this.Controls.Add(this.labelTamañoPizza);
            this.Controls.Add(this.labelMasaPizza);
            this.Controls.Add(this.labelListaIngredientes);
            this.Controls.Add(this.labelSeleccionPizza);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelCartel);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormPizzeria";
            this.Text = "Pizzeria";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCartel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labelSeleccionPizza;
        private System.Windows.Forms.Label labelListaIngredientes;
        private System.Windows.Forms.Label labelMasaPizza;
        private System.Windows.Forms.Label labelTamañoPizza;
        private System.Windows.Forms.ComboBox comboBoxMenuPizza;
        private System.Windows.Forms.ComboBox comboBoxMasa;
        private System.Windows.Forms.ComboBox comboBoxTamaño;
        private System.Windows.Forms.CheckedListBox checkedListBoxIngredientes;
        private System.Windows.Forms.Button buttonPedirPizza;
        private System.Windows.Forms.RichTextBox richTextBoxResumenDeCompra;
        private System.Windows.Forms.Label labelListaResumenDeCompra;
        private System.Windows.Forms.Label labelIngresientesPorDefecto;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.Button buttonImporteFinal;
        private System.Windows.Forms.CheckedListBox checkedListBoxIngredientesDefecto;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Label labelInfo;
    }
}

