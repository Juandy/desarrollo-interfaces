﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica2_VB_Pizzeria
{
    public partial class FormPizzeria : Form
    {
        public FormPizzeria()
        {
            InitializeComponent();
            comboBoxMenuPizza.SelectedIndex = 0;
            comboBoxTamaño.SelectedIndex = 0;
            comboBoxMasa.SelectedIndex = 0;
        }

        private void InicializaOpciones()
        {
            checkedListBoxIngredientes.Enabled = true;
            comboBoxMasa.Enabled = true;
            comboBoxTamaño.Enabled = true;
            labelIngresientesPorDefecto.Enabled = true;
            richTextBoxResumenDeCompra.Visible = false;
            labelListaResumenDeCompra.Visible = false;
            buttonImporteFinal.Visible = false;
            buttonVolver.Visible = false;

            foreach (int i in checkedListBoxIngredientes.CheckedIndices)
            {
                checkedListBoxIngredientesDefecto.Items.Clear();
                checkedListBoxIngredientes.Items.Clear();

                checkedListBoxIngredientes.Enabled = true;
                comboBoxMasa.Enabled = true;

                comboBoxMasa.SelectedIndex = 0;
                comboBoxTamaño.SelectedIndex = 0;
            }

            if (comboBoxMenuPizza.SelectedIndex == 0)
            {
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientesDefecto.Items.Clear();

                buttonPedirPizza.Visible = true;
                checkedListBoxIngredientesDefecto.Visible = false;
                labelIngresientesPorDefecto.Visible = false;
                checkedListBoxIngredientes.Enabled = false;
                comboBoxMasa.Enabled = false;
                comboBoxTamaño.Enabled = false;
            }
            if (comboBoxMenuPizza.SelectedIndex == 1)
            {
                checkedListBoxIngredientes.Visible = true;
                checkedListBoxIngredientesDefecto.Items.Clear();
                checkedListBoxIngredientesDefecto.Items.AddRange(new object[] {
                   "Jamón York",
                   "Champiñones"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                    "Jamón York [1€]","Champiñones [1€]","Calabacín [1€]",
                    "Aceitunas [1€]","Queso Mozzarella [1€]","Queso Azul [1€]",
                    "Queso Provolone [1€]","Queso Parmesano [1€]","Orégano [1€]",
                    "Queso de Cabra [1€]","Queso Gouda [1€]","Queso Roquefort [1€]",
                    "Albahaca [1€]", "Pimienta [1€]","Pepperoni [1€]",
                    "Pollo [1€]","Cebolla [1€]","Bacon [1€]",
                    "Carne Picada [1€]","Tomate [1€]","Setas [1€]"
                });
                labelInfo.Visible = false;
                buttonPedirPizza.Visible = true;
                labelIngresientesPorDefecto.Visible = true;
                checkedListBoxIngredientesDefecto.Visible = true;
                checkedListBoxIngredientes.Enabled = true;

                checkedListBoxIngredientesDefecto.SetItemChecked(0, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(1, true);

                comboBoxTamaño.SelectedIndex = 0;
                comboBoxMasa.SelectedIndex = 0;
            }
            if (comboBoxMenuPizza.SelectedIndex == 2)
            {
                checkedListBoxIngredientes.Visible = true;
                checkedListBoxIngredientesDefecto.Items.Clear();
                checkedListBoxIngredientesDefecto.Items.AddRange(new object[] {
                   "Calabacín",
                   "Aceitunas"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                   "Champiñones [1€]",
                   "Orégano [1€]",
                   "Cebolla [1€]",
                   "Tomate [1€]",
                   "Albahaca [1€]",
                   "Pimienta [1€]",
                   "Setas [1€]"
                });
                labelInfo.Visible = false;
                buttonPedirPizza.Visible = true;
                labelIngresientesPorDefecto.Visible = true;
                checkedListBoxIngredientesDefecto.Visible = true;
                checkedListBoxIngredientesDefecto.SetItemChecked(0, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(1, true);
            }
            if (comboBoxMenuPizza.SelectedIndex == 3)
            {
                checkedListBoxIngredientes.Visible = true;
                checkedListBoxIngredientesDefecto.Items.Clear();
                checkedListBoxIngredientesDefecto.Items.AddRange(new object[] {
                    "Tabasco",
                    "Ternera"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                    "Jamón York [1€]","Champiñones [1€]","Calabacín [1€]",
                    "Aceitunas [1€]","Queso Mozzarella [1€]","Queso Azul [1€]",
                    "Queso Provolone [1€]","Queso Parmesano [1€]","Orégano [1€]",
                    "Queso de Cabra [1€]","Queso Gouda [1€]","Queso Roquefort [1€]",
                    "Albahaca [1€]", "Pimienta [1€]","Pepperoni [1€]",
                    "Pollo [1€]","Cebolla [1€]","Bacon [1€]",
                    "Carne Picada [1€]","Tomate [1€]","Setas [1€]"
                });
                labelInfo.Visible = false;
                buttonPedirPizza.Visible = true;
                checkedListBoxIngredientesDefecto.Visible = true;
                labelIngresientesPorDefecto.Visible = true;

                comboBoxTamaño.SelectedIndex = 0;
                comboBoxMasa.SelectedIndex = 0;

                checkedListBoxIngredientesDefecto.SetItemChecked(0, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(1, true);
            }
            if (comboBoxMenuPizza.SelectedIndex == 4)
            {
                checkedListBoxIngredientes.Visible = true;
                checkedListBoxIngredientesDefecto.Items.Clear();
                checkedListBoxIngredientesDefecto.Items.AddRange(new object[] {
                  "Queso Mozzarella",
                  "Queso Azul",
                  "Queso Provolone",
                  "Queso Parmesano"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                "",
                "",
                });
                checkedListBoxIngredientes.Visible = false;
                labelInfo.Visible = true;
                buttonPedirPizza.Visible = true;
                checkedListBoxIngredientesDefecto.Visible = true;
                labelIngresientesPorDefecto.Visible = true;
                checkedListBoxIngredientes.Enabled = false;

                comboBoxMasa.Enabled = false;
                comboBoxTamaño.Enabled = true;
                comboBoxTamaño.SelectedIndex = 0;
                comboBoxMasa.SelectedIndex = 1;

                checkedListBoxIngredientes.SetItemChecked(0, true);
                checkedListBoxIngredientes.SetItemChecked(1, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(0, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(1, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(2, true);
                checkedListBoxIngredientesDefecto.SetItemChecked(3, true);
            }
        }

        public void CompruebaSeleccionComponentes()
        {
            bool encontrado = false;

            for (int i = 0; (i < checkedListBoxIngredientes.Items.Count) && (encontrado == false); i++)
            {
                if (checkedListBoxIngredientes.GetItemChecked(i) || (checkedListBoxIngredientes.CheckedItems.Count == 0))
                {
                    if ((checkedListBoxIngredientes.CheckedItems.Count >= 2) && (checkedListBoxIngredientes.CheckedItems.Count <= 4))
                    {
                        if ((comboBoxMenuPizza.SelectedIndex > 0) && (comboBoxMasa.SelectedIndex > 0) && (comboBoxTamaño.SelectedIndex > 0))
                        {
                            MostrarIngredientesResumen();
                            richTextBoxResumenDeCompra.Visible = true;
                            labelListaResumenDeCompra.Visible = true;
                            buttonImporteFinal.Visible = true;
                            buttonVolver.Visible = true;
                            encontrado = true;
                        }
                    }
                    else if ((checkedListBoxIngredientes.CheckedItems.Count == 0))
                    {
                        if ((comboBoxMenuPizza.SelectedIndex > 0) && (comboBoxMasa.SelectedIndex > 0) && (comboBoxTamaño.SelectedIndex > 0))
                        {
                            MostrarIngredientesResumen();
                            richTextBoxResumenDeCompra.Visible = true;
                            labelListaResumenDeCompra.Visible = true;
                            buttonImporteFinal.Visible = true;
                            buttonVolver.Visible = true;
                            encontrado = true;
                        }
                    }
                    else
                    {
                        string mensage = "Si quieres añadir ingredientes extra debes seleccionar mínimo 2 y máximo 4 .";
                        string titulo = "Ingredientes Extra";
                        MessageBoxButtons opciones = MessageBoxButtons.OK;
                        DialogResult result = MessageBox.Show(mensage, titulo, opciones,
                        MessageBoxIcon.Error);
                        encontrado = true;
                    }
                }
            }
            if (encontrado == false)
            {
                string mensage = "Para continuar con el pedido debes seleccionar el tipo de pizza que desees";
                string titulo = "Campos no seleccionados";
                MessageBoxButtons opciones = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(mensage, titulo, opciones,
                MessageBoxIcon.Error);
            }
        }
        public void MostrarIngredientesResumen()
        {
            string[] ingredientes_extras = new string[checkedListBoxIngredientes.Items.Count];
            string[] ingredientes_predet = new string[checkedListBoxIngredientesDefecto.Items.Count];

            richTextBoxResumenDeCompra.Text = "\n PEDIDO SOLICITADO: \n\t - Pizza: \n\t · "
                + comboBoxMenuPizza.SelectedItem + "\n\n\t - Ingredientes principales: \n\t";

            for (int i = 0; i < checkedListBoxIngredientesDefecto.Items.Count; i++)
            {
                if (checkedListBoxIngredientesDefecto.GetItemChecked(i))
                {
                    ingredientes_predet[i] = checkedListBoxIngredientesDefecto.Items[i].ToString();
                    richTextBoxResumenDeCompra.Text += " · " + ingredientes_predet[i] + "\n\t";
                }
            }

            for (int i = 0; i < checkedListBoxIngredientes.Items.Count; i++)
            {
                if (checkedListBoxIngredientes.GetItemChecked(i))
                {
                    ingredientes_extras[i] = checkedListBoxIngredientes.Items[i].ToString();
                    richTextBoxResumenDeCompra.Text += "Ingrediente extra\n\t\t" + " * " +ingredientes_extras[i] + "\n\t";
                }
            }
            richTextBoxResumenDeCompra.Text += "\n\t - Tamaño: \n\t · " + comboBoxTamaño.SelectedItem + "\n\n\t - Masa: \n\t · " + comboBoxMasa.SelectedItem + "\n\n";
        }

        private void comboBoxMenuPizza_SelectedIndexChanged(object sender, EventArgs e)
        {
            InicializaOpciones();
        }
        private void buttonPedirPizza_Click(object sender, EventArgs e)
        {
            CompruebaSeleccionComponentes();
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            richTextBoxResumenDeCompra.Visible = false;
            labelListaResumenDeCompra.Visible = false;
            buttonImporteFinal.Visible = false;
            buttonVolver.Visible = false;
            richTextBoxResumenDeCompra.Text = " ";
            comboBoxMenuPizza.Visible = true;
            checkedListBoxIngredientes.Visible = true;
            comboBoxTamaño.Visible = true;
            comboBoxMasa.Visible = true;
            labelIngresientesPorDefecto.Visible = true;
            buttonPedirPizza.Visible = true;
            checkedListBoxIngredientesDefecto.Visible = true;
        }

        private void buttonImporte_Click(object sender, EventArgs e)
        {
            double resultadoPrecio = 0;

            double pPizza = 0;
            double pTamaño = 0;
            double pMasa = 0;
            double pIngredientes = 0;

            double pizzaNewYork = 3.5;
            double pizzaVegetariana = 2;
            double pizzaBarbacoaPicante = 4.5;
            double pizzaCuatroQuesos = 3;

            double PtPequeño = 3;
            double PtMediano = 4.5;
            double PtFamiliar = 6.5;

            double PFina = 0.5;
            double PPan = 1;
            double PTradicional = 1.5;
            double PBordesRellenos = 3;

            //PIZZAS
            if (comboBoxMenuPizza.SelectedIndex == 1)
            {
                pPizza = pizzaNewYork;
            }
            if (comboBoxMenuPizza.SelectedIndex == 2)
            {
                pPizza = pizzaVegetariana;
            }
            if (comboBoxMenuPizza.SelectedIndex == 3)
            {
                pPizza = pizzaBarbacoaPicante;
            }
            if (comboBoxMenuPizza.SelectedIndex == 4)
            {
                pPizza = pizzaCuatroQuesos;
            }

            //TAMAÑO
            if (comboBoxTamaño.SelectedIndex == 1)
            {
                pTamaño = PtPequeño;
            }
            if (comboBoxTamaño.SelectedIndex == 2)
            {
                pTamaño = PtMediano;
            }
            if (comboBoxTamaño.SelectedIndex == 3)
            {
                pTamaño = PtFamiliar;
            }

            //MASA
            if (comboBoxMasa.SelectedIndex == 1)
            {
                pMasa = PFina;
            }
            if (comboBoxMasa.SelectedIndex == 2)
            {
                pMasa = PPan;
            }
            if (comboBoxMasa.SelectedIndex == 3)
            {
                pMasa = PTradicional;
            }
            if (comboBoxMasa.SelectedIndex == 4)
            {
                pMasa = PBordesRellenos;
            }

            //INGREDIENTES
            pIngredientes = checkedListBoxIngredientes.CheckedItems.Count;
            resultadoPrecio = pPizza + pIngredientes + pTamaño + pMasa;

            string mensage = "El importe total de su pedido es: " + resultadoPrecio + "€ \n\n";
            string titulo = "Total a pagar";
            MessageBoxButtons opciones = MessageBoxButtons.OK;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones);
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            string mensage = "¿Realmente quieres salir?";
            string titulo = "Salir del programa";
            MessageBoxButtons opciones = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones,
            MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
