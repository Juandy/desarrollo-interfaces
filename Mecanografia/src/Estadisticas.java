import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import jdk.jfr.events.FileWriteEvent;
import sun.invoke.empty.Empty;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class Estadisticas {

	private JFrame frmEstadisticas;

	public JFrame getFrmEstadisticas() {
		return frmEstadisticas;
	}

	public void setFrmEstadisticas(JFrame frmEstadisticas) {
		this.frmEstadisticas = frmEstadisticas;
	}

	String Usuario;

	// Imagenes
	Image img = new ImageIcon("IMG\\estadisticas.png").getImage();

	MenuEleccion principal;

	public Estadisticas(String usuario) {
		Usuario = usuario;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEstadisticas = new JFrame();
		frmEstadisticas.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		frmEstadisticas.setBounds(500, 150, 847, 615);
		frmEstadisticas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEstadisticas.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 574, 565);
		frmEstadisticas.getContentPane().add(scrollPane);

		JTextArea textAreaEstadisticas = new JTextArea();
		scrollPane.setViewportView(textAreaEstadisticas);
		textAreaEstadisticas.setEditable(false);
		textAreaEstadisticas.setText("");

		JLabel lblFoto = new JLabel("");
		lblFoto.setIcon(new ImageIcon(img.getScaledInstance(219, 217, Image.SCALE_SMOOTH)));
		lblFoto.setBounds(594, 86, 219, 217);
		frmEstadisticas.getContentPane().add(lblFoto);

		JButton btnMostrar = new JButton("Mostrar estadisticas");
		btnMostrar.setBackground(Color.LIGHT_GRAY);
		btnMostrar.setBorder(new LineBorder(Color.RED));
		btnMostrar.setBounds(594, 11, 219, 64);
		frmEstadisticas.getContentPane().add(btnMostrar);

		JButton btnLimpiarEstadisticas = new JButton("Limpiar estadisticas");
		btnLimpiarEstadisticas.setBackground(Color.LIGHT_GRAY);
		btnLimpiarEstadisticas.setBorder(new LineBorder(Color.RED));
		btnLimpiarEstadisticas.setBounds(594, 314, 219, 64);
		frmEstadisticas.getContentPane().add(btnLimpiarEstadisticas);

		JButton btnVolver = new JButton("Volver");
		btnVolver.setBorder(new LineBorder(Color.RED));
		btnVolver.setBackground(Color.LIGHT_GRAY);
		btnVolver.setBounds(594, 471, 219, 64);
		frmEstadisticas.getContentPane().add(btnVolver);

		JButton btnBorrarEstadisticas = new JButton("Borrar Estadisticas");
		btnBorrarEstadisticas.setBorder(new LineBorder(Color.RED));
		btnBorrarEstadisticas.setBackground(Color.LIGHT_GRAY);
		btnBorrarEstadisticas.setBounds(594, 389, 219, 64);
		frmEstadisticas.getContentPane().add(btnBorrarEstadisticas);

		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 831, 576);
		lblFondo.setHorizontalAlignment(SwingConstants.CENTER);
		lblFondo.setBackground(Color.BLACK);
		lblFondo.setIcon(new ImageIcon("IMG\\fondoLogin.jpg"));
		frmEstadisticas.getContentPane().add(lblFondo);

		// Eventos
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textAreaEstadisticas.setText("");
				File fichero = new File("FILES\\estadisticas.txt");
				String resultado = "";

				try {
					BufferedReader leer = new BufferedReader(new FileReader(fichero));
					String linea = leer.readLine();
					while (linea != null) {
						textAreaEstadisticas.append(linea + "\n");
						linea = leer.readLine();

					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		btnLimpiarEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textAreaEstadisticas.setText("");
			}
		});

		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MenuEleccion ME = new MenuEleccion(null);
				ME.getFrmEleccion().setVisible(true);
				frmEstadisticas.dispose();
			}
		});

		btnBorrarEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileWriter fw;
				int respuesta = JOptionPane.showConfirmDialog(frmEstadisticas,
						"Realmente quieres deseas borrar los datos? \nNOTA: Tambien borraras la de otros usuario", "Estadisticas",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0) {
					try {
						fw = new FileWriter("FILES\\estadisticas.txt");
						PrintWriter pw = new PrintWriter(fw);

						pw.write("");
						pw.flush();
						pw.close();
					}
					catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

		});
	}
}
