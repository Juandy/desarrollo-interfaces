import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class AcercaDe {

	private JFrame frmAcercaDe;
	
	public JFrame getFrmAcercaDe() {
		return frmAcercaDe;
	}

	public void setFrmAcercaDe(JFrame frmAcercaDe) {
		this.frmAcercaDe = frmAcercaDe;
	}
	
	//Imagenes
	Image img = new ImageIcon("IMG\\fondoLogin.jpg").getImage();
	
	//llamadas
	Login principal;

	public AcercaDe(Login principal) {
		initialize();
		this.principal=principal;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAcercaDe = new JFrame();
		frmAcercaDe.setTitle("Informaci\u00F3n");
		frmAcercaDe.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		frmAcercaDe.setBounds(100, 100, 450, 300);
		frmAcercaDe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.setBounds(10, 227, 89, 23);
		btnCerrar.setHorizontalAlignment(SwingConstants.LEFT);
		btnCerrar.setForeground(Color.WHITE);
		btnCerrar.setBorder(null);
		btnCerrar.setFocusPainted(false);
		btnCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnCerrar.setFont(new Font("Yu Gothic UI", Font.PLAIN, 20));
		btnCerrar.setContentAreaFilled(false);
		btnCerrar.setIcon(null);
		frmAcercaDe.getContentPane().add(btnCerrar);
		
		JLabel lblCreador = new JLabel("Creardor");
		lblCreador.setForeground(Color.WHITE);
		lblCreador.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblCreador.setBounds(63, 61, 110, 23);
		frmAcercaDe.getContentPane().add(lblCreador);

		JLabel lblVersion = new JLabel("Version");
		lblVersion.setForeground(Color.WHITE);
		lblVersion.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblVersion.setBounds(63, 95, 110, 23);
		frmAcercaDe.getContentPane().add(lblVersion);

		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setForeground(Color.WHITE);
		lblFecha.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblFecha.setBounds(63, 129, 110, 23);
		frmAcercaDe.getContentPane().add(lblFecha);

		JLabel lblNombre = new JLabel("Juan Diego Fernandez ");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblNombre.setBounds(163, 61, 271, 23);
		frmAcercaDe.getContentPane().add(lblNombre);

		JLabel lbldatos1 = new JLabel("1.0.0");
		lbldatos1.setForeground(Color.WHITE);
		lbldatos1.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lbldatos1.setBounds(163, 95, 190, 23);
		frmAcercaDe.getContentPane().add(lbldatos1);

		JLabel lbldatos2 = new JLabel("1/10/2019");
		lbldatos2.setForeground(Color.WHITE);
		lbldatos2.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lbldatos2.setBounds(163, 129, 190, 23);
		frmAcercaDe.getContentPane().add(lbldatos2);

		JLabel lblFondo = new JLabel("");
		lblFondo.setHorizontalAlignment(SwingConstants.CENTER);
		lblFondo.setIcon(new ImageIcon(img.getScaledInstance(450, 300, Image.SCALE_SMOOTH)));
		lblFondo.setBounds(0, 0, 434, 261);
		frmAcercaDe.getContentPane().add(lblFondo);

		//Eventos
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAcercaDe.dispose();
			}
		});
	}
}
