import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class MenuEleccion {

	private JFrame frmEleccion;
	private JLabel lblFondo;
	
	String Leccion = "";
	String usuario = "";
	
	public JFrame getFrmEleccion() {
		return frmEleccion;
	}

	public void setFrmEleccion(JFrame frmEleccion) {
		this.frmEleccion = frmEleccion;
	}
	
	//Imagenes
	Image img = new ImageIcon("IMG\\info.png").getImage();

	public MenuEleccion(String Usuario) {
		usuario = Usuario;
		initialize();
	}
	private void initialize() {
		frmEleccion = new JFrame();
		frmEleccion.setResizable(false);
		frmEleccion.setTitle("Mecano.exe");
		frmEleccion.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		frmEleccion.setBounds(500, 150, 847, 615);
		frmEleccion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEleccion.getContentPane().setLayout(null);
		
		JTextArea txtAInfo = new JTextArea();
		txtAInfo.setFont(new Font("Yu Gothic UI", Font.BOLD, 20));
		txtAInfo.setForeground(Color.WHITE);
		txtAInfo.setOpaque(false);
		txtAInfo.setTabSize(5);
		txtAInfo.setText("Hola!! Bienvenido a mecano.exe por favor es necesario que elijas tu nivel \r\nde dificultas entre las dos opciones:\r\n\r\nLecci\u00F3n 1 -  Abecedario\r\nLecci\u00F3n 2 - Texto avanzado\r\n");
		txtAInfo.setBounds(10, 11, 720, 154);
		frmEleccion.getContentPane().add(txtAInfo);
		
		JLabel lblimg = new JLabel("");
		lblimg.setIcon(new ImageIcon(img.getScaledInstance(485, 440, Image.SCALE_SMOOTH)));
		lblimg.setBounds(321, 94, 485, 440);
		frmEleccion.getContentPane().add(lblimg);
		
		JButton btnLeccin = new JButton("Lecci\u00F3n 1");
		btnLeccin.setBounds(10, 176, 204, 51);
		btnLeccin.setFocusPainted(false);
		btnLeccin.setFocusTraversalKeysEnabled(false);
		frmEleccion.getContentPane().add(btnLeccin);
		
		JButton btnLeccin_1 = new JButton("Lecci\u00F3n 2");
		btnLeccin_1.setBounds(10, 238, 204, 51);
		btnLeccin_1.setFocusPainted(false);
		btnLeccin_1.setFocusTraversalKeysEnabled(false);
		frmEleccion.getContentPane().add(btnLeccin_1);
		
		JButton btnCargarEstadisticas = new JButton("Estadisticas");
		btnCargarEstadisticas.setBounds(10, 300, 204, 51);
		btnCargarEstadisticas.setFocusPainted(false);
		btnCargarEstadisticas.setFocusTraversalKeysEnabled(false);
		frmEleccion.getContentPane().add(btnCargarEstadisticas);
		
		JButton btnCambiarUsuario = new JButton("Cambiar Usuario");
		btnCambiarUsuario.setBounds(602, 524, 204, 51);
		btnCambiarUsuario.setFocusPainted(false);
		btnCambiarUsuario.setFocusTraversalKeysEnabled(false);
		frmEleccion.getContentPane().add(btnCambiarUsuario);
		
		lblFondo = new JLabel("");
		lblFondo.setHorizontalAlignment(SwingConstants.CENTER);
		lblFondo.setBackground(Color.BLACK);
		lblFondo.setIcon(new ImageIcon("IMG\\fondoLogin.jpg"));
		lblFondo.setBounds(0, 0, 831, 615);
		frmEleccion.getContentPane().add(lblFondo);
		
		//Eventos
		btnLeccin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UsuariosTextos leer = new UsuariosTextos();
				try {
					Leccion = leer.Contenido("FILES\\Texto.txt", "Leccion 1");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Principal cargar = new Principal(Leccion, "Leccion 1", usuario);
				cargar.getPrincipal().setVisible(true);
				frmEleccion.dispose();
			}
		});
		
		btnLeccin_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UsuariosTextos  leer = new UsuariosTextos();
				try {
					Leccion = leer.Contenido("FILES\\Texto.txt", "Leccion 2");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Principal cargar = new Principal(Leccion, "Leccion 2", usuario);
				cargar.getPrincipal().setVisible(true);
				frmEleccion.dispose();
			}
		});
		
		btnCambiarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login login = new Login();
				login.getFrmLogin().setVisible(true);
				frmEleccion.dispose();
				
			}
		});
		
		btnCargarEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Estadisticas estadisticas = new Estadisticas(usuario);
				estadisticas.getFrmEstadisticas().setVisible(true);
				frmEleccion.dispose();
				
			}
		});
		
	}
}
