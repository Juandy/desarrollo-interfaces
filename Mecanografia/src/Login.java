import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class Login {

	private JFrame frmLogin;
	public JFrame getFrmLogin() {
		return frmLogin;
	}

	public void setFrmLogin(JFrame frmLogin) {
		this.frmLogin = frmLogin;
	}

	private JLabel lblFondo;
	private JLabel lblTitulo;
	private JLabel lblContenido;
	private JLabel lblIcon;
	private JLabel lblIcon2;
	private JTextField textFileUsuario;
	private JPasswordField contrasena;
	private JButton btnAcercaDe;
	
	AcercaDe acercaDe = new AcercaDe(this);
	
	//Imagenes
		Image img = new ImageIcon("IMG\\\\iconUser.png").getImage();
		Image img2 = new ImageIcon("IMG\\\\iconPwd.png").getImage();
		
	//Funciones
		public void acceder () {
			boolean login = false;
			String usuario = textFileUsuario.getText();
			String passwd = String.copyValueOf(contrasena.getPassword());
			
			try {
				UsuariosTextos user= new UsuariosTextos();
				login = user.busca (usuario, passwd, "FILES\\usuarios.txt", login);
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			if(login) {
				
				MenuEleccion e = new MenuEleccion(usuario);
				e.getFrmEleccion().setVisible(true);
				frmLogin.dispose();
			} else
				JOptionPane.showMessageDialog(null, "Login incorrecto", "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setTitle("Mecano.exe");
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		frmLogin.setResizable(false);
		frmLogin.setBounds(500, 150, 853, 633);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		lblTitulo = new JLabel("LOGIN");
		lblTitulo.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblTitulo.setBackground(Color.BLACK);
		lblTitulo.setFont(new Font("Yu Gothic UI", Font.BOLD, 30));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(256, 159, 302, 56);
		frmLogin.getContentPane().add(lblTitulo);
		
		textFileUsuario = new JTextField();
		textFileUsuario.setBorder(new LineBorder(Color.BLACK));
		textFileUsuario.setBounds(296, 270, 250, 36);
		textFileUsuario.setColumns(10);
		frmLogin.getContentPane().add(textFileUsuario);

		contrasena = new JPasswordField();
		contrasena.setBorder(new LineBorder(Color.BLACK));
		contrasena.setBounds(296, 326, 250, 36);
		contrasena.setColumns(10);
		frmLogin.getContentPane().add(contrasena);
		
		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.setBackground(Color.LIGHT_GRAY);
		btnContinuar.setBorder(new LineBorder(Color.RED));
		btnContinuar.setFocusPainted(false);
		btnContinuar.setFocusTraversalKeysEnabled(false);
		btnContinuar.setBounds(348, 396, 126, 41);
		frmLogin.getContentPane().add(btnContinuar);
		
		lblIcon = new JLabel("");
		lblIcon.setIcon(new ImageIcon(img.getScaledInstance(50, 50, Image.SCALE_SMOOTH)));
		lblIcon.setBounds(256, 265, 50, 50);
		frmLogin.getContentPane().add(lblIcon);

		lblIcon2 = new JLabel("");
		lblIcon2.setIcon(new ImageIcon(img2.getScaledInstance(50, 50, Image.SCALE_SMOOTH)));
		lblIcon2.setBounds(256, 317, 50, 50);
		frmLogin.getContentPane().add(lblIcon2);
		
		lblContenido = new JLabel("");
		lblContenido.setFocusTraversalKeysEnabled(false);
		lblContenido.setFont(new Font("Yu Gothic UI", Font.BOLD, 11));
		lblContenido.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblContenido.setBounds(256, 226, 302, 259);
		frmLogin.getContentPane().add(lblContenido);
		
		btnAcercaDe = new JButton("Acerca de");
		btnAcercaDe.setHorizontalAlignment(SwingConstants.LEFT);
		btnAcercaDe.setForeground(Color.WHITE);
		btnAcercaDe.setBorder(null);
		btnAcercaDe.setFocusPainted(false);
		btnAcercaDe.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnAcercaDe.setFont(new Font("Yu Gothic UI", Font.PLAIN, 20));
		btnAcercaDe.setContentAreaFilled(false);
		btnAcercaDe.setIcon(null);
		btnAcercaDe.setBounds(10, 11, 153, 23);
		frmLogin.getContentPane().add(btnAcercaDe);
		
		lblFondo = new JLabel("");
		lblFondo.setHorizontalAlignment(SwingConstants.CENTER);
		lblFondo.setBackground(Color.BLACK);
		lblFondo.setIcon(new ImageIcon("IMG\\fondoLogin.jpg"));
		lblFondo.setBounds(0, 0, 847, 604);
		frmLogin.getContentPane().add(lblFondo);
		
		//Eventos
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				acceder();
			}
		});
		
		btnAcercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				acercaDe.getFrmAcercaDe().setVisible(true);
			}
		});
		contrasena.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					acceder();			
			}
		});
		
		textFileUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					acceder();
			}
		});

	}
}
