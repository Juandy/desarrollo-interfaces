import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.MatteBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

import javafx.scene.control.ButtonBar.ButtonData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;

import javax.swing.Timer;

import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTextPane;

public class Principal {

	private JFrame frmPrincipal;
	int pulsaciones = 0;
	JLabel lblContador = new JLabel("00:00");
	JLabel lblPTotalesRes = new JLabel("0");
	JLabel lblPPMRes = new JLabel("0");
	JLabel lblErroresRes = new JLabel("0");

	JLabel label = null;
	JLabel labelaux = null;
	String Leccion;
	String NumeroLeccion;
	String pTotales, ppm;
	String Usuario;

	java.util.Date fecha = new Date();

	char[] aCaracteres;
	int sec = 0, min = 0, contadorletras = 0, errores = 0;
	boolean start = false, stoplec = false;
	Timer timer;
	JTextArea textAreaTexto = new JTextArea();

	public JFrame getPrincipal() {
		return frmPrincipal;
	}

	public void setPrincipal(JFrame principal) {
		this.frmPrincipal = principal;
	}

	static int cnt = 0;

	static DefaultHighlighter.DefaultHighlightPainter highlightResaltarCorrecto = new DefaultHighlighter.DefaultHighlightPainter(
			Color.green);
	static DefaultHighlighter.DefaultHighlightPainter highlightResaltarError = new DefaultHighlighter.DefaultHighlightPainter(
			Color.red);

	//Hightlighter
	public static void correcto(JTextArea textAreaTexto) {
		try {
			textAreaTexto.getHighlighter().addHighlight(cnt, cnt + 1, highlightResaltarCorrecto);
			cnt++;
		} catch (BadLocationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static void error(JTextArea textAreaTexto) {

		try {
			textAreaTexto.getHighlighter().addHighlight(cnt, cnt + 1, highlightResaltarError);
			cnt++;
		} catch (BadLocationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


	}

	// Funciones
	//
	public void Contador(JLabel lblContador) {
		// Aqu� se pone en marcha el timer cada segundo.
		timer = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				sec++;

				if (stoplec)
					timer.stop();

				if (sec == 60) {
					min += 1;
					sec = 0;
				}

				if (sec < 10)
					lblContador.setText("0" + min + ":" + "0" + sec);
				else
					lblContador.setText("0" + min + ":" + sec);
				if (min == 5) {
					timer.stop();
				}

				pTotales = Integer.toString(contadorletras);
				lblPTotalesRes.setText(pTotales);
				ppm = String.valueOf((60 * Integer.parseInt(pTotales)) / 60);
				lblPPMRes.setText(ppm);

			}
		});

		timer.start();
	}

	//
	public void GuardarEstadisticas() {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			String estadisticas = "FILES\\estadisticas.txt";
			fichero = new FileWriter(estadisticas, true);
			pw = new PrintWriter(fichero);
			pw.println("\nLeccion: " + NumeroLeccion + "\nFecha: " + fecha + "\nPulsaciones totales: " + pTotales
					+ "\nPulsaciones por minuto: " + ppm + "\nErrores: " + errores + "\nTiempo: " + min + ":" + sec
					+ "\n Usario: " + Usuario);

		} catch (Exception er) {
			er.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	//
	public void Resaltar(char letrapulsada, JLabel label) {

		if (aCaracteres.length == contadorletras) {
			stoplec = true;
			JOptionPane.showMessageDialog(null, "Enhorabuena! Has terminado tu leccion.");

		}

		if (!start) {
			Contador(lblContador);
			start = true;
		}

		if (contadorletras != 0)
			labelaux.setBackground(SystemColor.control);

		if (aCaracteres[contadorletras] == letrapulsada) {
			label.setBackground(Color.GREEN);
			correcto(textAreaTexto);

		} else {
			label.setBackground(Color.RED);
			errores++;
			String err = Integer.toString(errores);
			lblErroresRes.setText(err);
			error(textAreaTexto);

		}

		labelaux = label;
		contadorletras++;
	}

	//
	public KeyListener Listener = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub

		}
	};

	// Initialize
	public Principal(String leccion, String NLeccion, String usuario) {
		Leccion = leccion;
		NumeroLeccion = NLeccion;
		Usuario = usuario;
		aCaracteres = Leccion.toCharArray();
		initialize();
	}

	private void initialize() {
		frmPrincipal = new JFrame();
		frmPrincipal.getContentPane().setBackground(Color.WHITE);
		frmPrincipal.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		// principal.setUndecorated(true);//Quitar barra superior
		frmPrincipal.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frmPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrincipal.setBounds(100, 100, 1935, 1050);
		frmPrincipal.getContentPane().setLayout(null);

		JTextPane textAreaLectura = new JTextPane();
		textAreaLectura.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(0, 0, 0)));
		textAreaLectura.addKeyListener(Listener);
		textAreaLectura.setBounds(10, 112, 1272, 337);
		frmPrincipal.getContentPane().add(textAreaLectura);

		textAreaTexto.setEditable(false);
		textAreaTexto.setText(Leccion);
		textAreaTexto.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(0, 0, 0)));
		textAreaTexto.setBounds(10, 11, 1272, 438);
		frmPrincipal.getContentPane().add(textAreaTexto);

		JLabel lblQ = new JLabel("Q");
		lblQ.setBackground(SystemColor.control);
		lblQ.setOpaque(true);
		lblQ.setHorizontalAlignment(SwingConstants.CENTER);
		lblQ.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblQ.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblQ.setBounds(144, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblQ);

		JLabel lblW = new JLabel("W");
		lblW.setOpaque(true);
		lblW.setHorizontalAlignment(SwingConstants.CENTER);
		lblW.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblW.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblW.setBounds(234, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblW);

		JLabel lblE = new JLabel("E");
		lblE.setOpaque(true);
		lblE.setHorizontalAlignment(SwingConstants.CENTER);
		lblE.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblE.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblE.setBounds(324, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblE);

		JLabel lblR = new JLabel("R");
		lblR.setOpaque(true);
		lblR.setHorizontalAlignment(SwingConstants.CENTER);
		lblR.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblR.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblR.setBounds(415, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblR);

		JLabel lblT = new JLabel("T");
		lblT.setOpaque(true);
		lblT.setHorizontalAlignment(SwingConstants.CENTER);
		lblT.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblT.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblT.setBounds(505, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblT);

		JLabel lblY = new JLabel("Y");
		lblY.setOpaque(true);
		lblY.setHorizontalAlignment(SwingConstants.CENTER);
		lblY.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblY.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblY.setBounds(595, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblY);

		JLabel lblU = new JLabel("U");
		lblU.setOpaque(true);
		lblU.setHorizontalAlignment(SwingConstants.CENTER);
		lblU.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblU.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblU.setBounds(685, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblU);

		JLabel lblI = new JLabel("I");
		lblI.setOpaque(true);
		lblI.setHorizontalAlignment(SwingConstants.CENTER);
		lblI.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblI.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblI.setBounds(775, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblI);

		JLabel lblO = new JLabel("O");
		lblO.setOpaque(true);
		lblO.setHorizontalAlignment(SwingConstants.CENTER);
		lblO.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblO.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblO.setBounds(865, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblO);

		JLabel lblP = new JLabel("P");
		lblP.setOpaque(true);
		lblP.setHorizontalAlignment(SwingConstants.CENTER);
		lblP.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblP.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblP.setBounds(954, 584, 80, 80);
		frmPrincipal.getContentPane().add(lblP);

		JLabel lblA = new JLabel("A");
		lblA.setOpaque(true);
		lblA.setHorizontalAlignment(SwingConstants.CENTER);
		lblA.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblA.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblA.setBounds(154, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblA);

		JLabel lblS = new JLabel("S");
		lblS.setOpaque(true);
		lblS.setHorizontalAlignment(SwingConstants.CENTER);
		lblS.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblS.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblS.setBounds(244, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblS);

		JLabel lblD = new JLabel("D");
		lblD.setOpaque(true);
		lblD.setHorizontalAlignment(SwingConstants.CENTER);
		lblD.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblD.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblD.setBounds(334, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblD);

		JLabel lblF = new JLabel("F");
		lblF.setOpaque(true);
		lblF.setHorizontalAlignment(SwingConstants.CENTER);
		lblF.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblF.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblF.setBounds(425, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblF);

		JLabel lblG = new JLabel("G");
		lblG.setOpaque(true);
		lblG.setHorizontalAlignment(SwingConstants.CENTER);
		lblG.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblG.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblG.setBounds(515, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblG);

		JLabel lblH = new JLabel("H");
		lblH.setOpaque(true);
		lblH.setHorizontalAlignment(SwingConstants.CENTER);
		lblH.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblH.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblH.setBounds(605, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblH);

		JLabel lblJ = new JLabel("J");
		lblJ.setOpaque(true);
		lblJ.setHorizontalAlignment(SwingConstants.CENTER);
		lblJ.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblJ.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblJ.setBounds(695, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblJ);

		JLabel lblK = new JLabel("K");
		lblK.setOpaque(true);
		lblK.setHorizontalAlignment(SwingConstants.CENTER);
		lblK.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblK.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblK.setBounds(785, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblK);

		JLabel lblL = new JLabel("L");
		lblL.setOpaque(true);
		lblL.setHorizontalAlignment(SwingConstants.CENTER);
		lblL.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblL.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblL.setBounds(875, 675, 80, 80);
		frmPrincipal.getContentPane().add(lblL);

		JLabel label_10 = new JLabel("\u00D1");
		label_10.setOpaque(true);
		label_10.setHorizontalAlignment(SwingConstants.CENTER);
		label_10.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_10.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_10.setBounds(964, 675, 80, 80);
		frmPrincipal.getContentPane().add(label_10);

		JLabel lblZ = new JLabel("Z");
		lblZ.setOpaque(true);
		lblZ.setHorizontalAlignment(SwingConstants.CENTER);
		lblZ.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblZ.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblZ.setBounds(190, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblZ);

		JLabel lblX = new JLabel("X");
		lblX.setOpaque(true);
		lblX.setHorizontalAlignment(SwingConstants.CENTER);
		lblX.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblX.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblX.setBounds(280, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblX);

		JLabel lblC = new JLabel("C");
		lblC.setOpaque(true);
		lblC.setHorizontalAlignment(SwingConstants.CENTER);
		lblC.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblC.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblC.setBounds(370, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblC);

		JLabel lblV = new JLabel("V");
		lblV.setOpaque(true);
		lblV.setHorizontalAlignment(SwingConstants.CENTER);
		lblV.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblV.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblV.setBounds(461, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblV);

		JLabel lblB = new JLabel("B");
		lblB.setOpaque(true);
		lblB.setHorizontalAlignment(SwingConstants.CENTER);
		lblB.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblB.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblB.setBounds(551, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblB);

		JLabel lblN = new JLabel("N");
		lblN.setOpaque(true);
		lblN.setHorizontalAlignment(SwingConstants.CENTER);
		lblN.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblN.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblN.setBounds(641, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblN);

		JLabel lblM = new JLabel("M");
		lblM.setOpaque(true);
		lblM.setHorizontalAlignment(SwingConstants.CENTER);
		lblM.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblM.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblM.setBounds(731, 765, 80, 80);
		frmPrincipal.getContentPane().add(lblM);

		JLabel label_coma = new JLabel(",");
		label_coma.setOpaque(true);
		label_coma.setHorizontalAlignment(SwingConstants.CENTER);
		label_coma.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_coma.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_coma.setBounds(821, 765, 80, 80);
		frmPrincipal.getContentPane().add(label_coma);

		JLabel label_punto = new JLabel(".");
		label_punto.setOpaque(true);
		label_punto.setHorizontalAlignment(SwingConstants.CENTER);
		label_punto.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_punto.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_punto.setBounds(911, 765, 80, 80);
		frmPrincipal.getContentPane().add(label_punto);

		JLabel label_11 = new JLabel("-");
		label_11.setOpaque(true);
		label_11.setHorizontalAlignment(SwingConstants.CENTER);
		label_11.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_11.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_11.setBounds(1000, 765, 80, 80);
		frmPrincipal.getContentPane().add(label_11);

		JLabel lblSPACE = new JLabel("");
		lblSPACE.setOpaque(true);
		lblSPACE.setHorizontalAlignment(SwingConstants.CENTER);
		lblSPACE.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSPACE.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblSPACE.setBounds(357, 856, 487, 80);
		frmPrincipal.getContentPane().add(lblSPACE);

		JLabel lbl1 = new JLabel("1");
		lbl1.setOpaque(true);
		lbl1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl1.setBackground(SystemColor.menu);
		lbl1.setBounds(100, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl1);

		JLabel lbl2 = new JLabel("2");
		lbl2.setOpaque(true);
		lbl2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl2.setBackground(SystemColor.menu);
		lbl2.setBounds(190, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl2);

		JLabel lbl3 = new JLabel("3");
		lbl3.setOpaque(true);
		lbl3.setHorizontalAlignment(SwingConstants.CENTER);
		lbl3.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl3.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl3.setBackground(SystemColor.menu);
		lbl3.setBounds(280, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl3);

		JLabel lbl4 = new JLabel("4");
		lbl4.setOpaque(true);
		lbl4.setHorizontalAlignment(SwingConstants.CENTER);
		lbl4.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl4.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl4.setBackground(SystemColor.menu);
		lbl4.setBounds(370, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl4);

		JLabel lbl5 = new JLabel("5");
		lbl5.setOpaque(true);
		lbl5.setHorizontalAlignment(SwingConstants.CENTER);
		lbl5.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl5.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl5.setBackground(SystemColor.menu);
		lbl5.setBounds(460, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl5);

		JLabel lbl6 = new JLabel("6");
		lbl6.setOpaque(true);
		lbl6.setHorizontalAlignment(SwingConstants.CENTER);
		lbl6.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl6.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl6.setBackground(SystemColor.menu);
		lbl6.setBounds(550, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl6);

		JLabel lbl7 = new JLabel("7");
		lbl7.setOpaque(true);
		lbl7.setHorizontalAlignment(SwingConstants.CENTER);
		lbl7.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl7.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl7.setBackground(SystemColor.menu);
		lbl7.setBounds(640, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl7);

		JLabel lbl8 = new JLabel("8");
		lbl8.setOpaque(true);
		lbl8.setHorizontalAlignment(SwingConstants.CENTER);
		lbl8.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl8.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl8.setBackground(SystemColor.menu);
		lbl8.setBounds(730, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl8);

		JLabel lbl9 = new JLabel("9");
		lbl9.setOpaque(true);
		lbl9.setHorizontalAlignment(SwingConstants.CENTER);
		lbl9.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl9.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl9.setBackground(SystemColor.menu);
		lbl9.setBounds(820, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl9);

		JLabel lbl0 = new JLabel("0");
		lbl0.setOpaque(true);
		lbl0.setHorizontalAlignment(SwingConstants.CENTER);
		lbl0.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl0.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl0.setBackground(SystemColor.menu);
		lbl0.setBounds(910, 493, 80, 80);
		frmPrincipal.getContentPane().add(lbl0);

		JLabel label_1 = new JLabel("\u00AA\r\n\u00BA \\\r\n");
		label_1.setOpaque(true);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_1.setBackground(SystemColor.menu);
		label_1.setBounds(10, 493, 80, 80);
		frmPrincipal.getContentPane().add(label_1);

		JLabel lblShift = new JLabel("TAB");
		lblShift.setOpaque(true);
		lblShift.setHorizontalAlignment(SwingConstants.CENTER);
		lblShift.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblShift.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblShift.setBackground(SystemColor.menu);
		lblShift.setBounds(10, 584, 124, 80);
		frmPrincipal.getContentPane().add(lblShift);

		JLabel lblBloqMayus = new JLabel("Bloq Mayus");
		lblBloqMayus.setOpaque(true);
		lblBloqMayus.setHorizontalAlignment(SwingConstants.CENTER);
		lblBloqMayus.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblBloqMayus.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblBloqMayus.setBackground(SystemColor.menu);
		lblBloqMayus.setBounds(10, 675, 134, 80);
		frmPrincipal.getContentPane().add(lblBloqMayus);

		JLabel lblShift_1 = new JLabel("SHIFT");
		lblShift_1.setOpaque(true);
		lblShift_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblShift_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblShift_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblShift_1.setBackground(SystemColor.menu);
		lblShift_1.setBounds(10, 766, 80, 80);
		frmPrincipal.getContentPane().add(lblShift_1);

		JLabel label_5 = new JLabel("< >");
		label_5.setOpaque(true);
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_5.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_5.setBackground(SystemColor.menu);
		label_5.setBounds(100, 766, 80, 80);
		frmPrincipal.getContentPane().add(label_5);

		JLabel lblCtrl = new JLabel("Ctrl");
		lblCtrl.setOpaque(true);
		lblCtrl.setHorizontalAlignment(SwingConstants.CENTER);
		lblCtrl.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCtrl.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblCtrl.setBackground(SystemColor.menu);
		lblCtrl.setBounds(10, 856, 98, 80);
		frmPrincipal.getContentPane().add(lblCtrl);

		JLabel lblWindows = new JLabel("Windows");
		lblWindows.setOpaque(true);
		lblWindows.setHorizontalAlignment(SwingConstants.CENTER);
		lblWindows.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWindows.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblWindows.setBackground(SystemColor.menu);
		lblWindows.setBounds(118, 856, 116, 80);
		frmPrincipal.getContentPane().add(lblWindows);

		JLabel lblAlt = new JLabel("Alt");
		lblAlt.setOpaque(true);
		lblAlt.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlt.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAlt.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblAlt.setBackground(SystemColor.menu);
		lblAlt.setBounds(244, 856, 103, 80);
		frmPrincipal.getContentPane().add(lblAlt);

		JLabel lblAltGr = new JLabel("Alt Gr");
		lblAltGr.setOpaque(true);
		lblAltGr.setHorizontalAlignment(SwingConstants.CENTER);
		lblAltGr.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAltGr.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblAltGr.setBackground(SystemColor.menu);
		lblAltGr.setBounds(854, 856, 118, 80);
		frmPrincipal.getContentPane().add(lblAltGr);

		JLabel lblFn = new JLabel("Fn");
		lblFn.setOpaque(true);
		lblFn.setHorizontalAlignment(SwingConstants.CENTER);
		lblFn.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblFn.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblFn.setBackground(SystemColor.menu);
		lblFn.setBounds(982, 856, 106, 80);
		frmPrincipal.getContentPane().add(lblFn);

		JLabel lblCtrl_1 = new JLabel("Ctrl");
		lblCtrl_1.setOpaque(true);
		lblCtrl_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblCtrl_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCtrl_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblCtrl_1.setBackground(SystemColor.menu);
		lblCtrl_1.setBounds(1214, 856, 106, 80);
		frmPrincipal.getContentPane().add(lblCtrl_1);

		JLabel lblShift_2 = new JLabel("SHIFT");
		lblShift_2.setOpaque(true);
		lblShift_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblShift_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblShift_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblShift_2.setBackground(SystemColor.menu);
		lblShift_2.setBounds(1090, 765, 230, 80);
		frmPrincipal.getContentPane().add(lblShift_2);

		JLabel lblWindows_1 = new JLabel("Windows");
		lblWindows_1.setOpaque(true);
		lblWindows_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblWindows_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWindows_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblWindows_1.setBackground(SystemColor.menu);
		lblWindows_1.setBounds(1098, 856, 106, 80);
		frmPrincipal.getContentPane().add(lblWindows_1);

		JLabel label_2 = new JLabel("\u00A8 \u00B4 {");
		label_2.setOpaque(true);
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_2.setBounds(1054, 675, 80, 80);
		frmPrincipal.getContentPane().add(label_2);

		JLabel label_3 = new JLabel("\u00E7 }");
		label_3.setOpaque(true);
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_3.setBounds(1144, 675, 80, 80);
		frmPrincipal.getContentPane().add(label_3);

		JLabel label_15 = new JLabel("");
		label_15.setOpaque(true);
		label_15.setHorizontalAlignment(SwingConstants.CENTER);
		label_15.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_15.setBorder(null);
		label_15.setBounds(1234, 596, 23, 80);
		frmPrincipal.getContentPane().add(label_15);

		JLabel label_4 = new JLabel("");
		label_4.setOpaque(true);
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_4.setBounds(1234, 584, 86, 171);
		frmPrincipal.getContentPane().add(label_4);

		JLabel label_6 = new JLabel("` ^ [");
		label_6.setOpaque(true);
		label_6.setHorizontalAlignment(SwingConstants.CENTER);
		label_6.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_6.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_6.setBounds(1044, 584, 80, 80);
		frmPrincipal.getContentPane().add(label_6);

		JLabel label_7 = new JLabel("+ * ]");
		label_7.setOpaque(true);
		label_7.setHorizontalAlignment(SwingConstants.CENTER);
		label_7.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_7.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_7.setBounds(1134, 584, 80, 80);
		frmPrincipal.getContentPane().add(label_7);

		JLabel label_12 = new JLabel("");
		label_12.setOpaque(true);
		label_12.setHorizontalAlignment(SwingConstants.CENTER);
		label_12.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_12.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_12.setBounds(1224, 584, 96, 80);
		frmPrincipal.getContentPane().add(label_12);

		JLabel label_13 = new JLabel("' ?");
		label_13.setOpaque(true);
		label_13.setHorizontalAlignment(SwingConstants.CENTER);
		label_13.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_13.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_13.setBackground(SystemColor.menu);
		label_13.setBounds(1000, 493, 80, 80);
		frmPrincipal.getContentPane().add(label_13);

		JLabel label_14 = new JLabel("\u00A1 \u00BF");
		label_14.setOpaque(true);
		label_14.setHorizontalAlignment(SwingConstants.CENTER);
		label_14.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_14.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_14.setBackground(SystemColor.menu);
		label_14.setBounds(1090, 493, 80, 80);
		frmPrincipal.getContentPane().add(label_14);

		JLabel lblDelete = new JLabel("DELETE");
		lblDelete.setOpaque(true);
		lblDelete.setHorizontalAlignment(SwingConstants.CENTER);
		lblDelete.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblDelete.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblDelete.setBackground(SystemColor.menu);
		lblDelete.setBounds(1180, 493, 140, 80);
		frmPrincipal.getContentPane().add(lblDelete);

		JLabel lblPTotales = new JLabel("Pulsaciones totales:");
		lblPTotales.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPTotales.setBounds(1475, 523, 198, 50);
		frmPrincipal.getContentPane().add(lblPTotales);

		lblPTotalesRes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPTotalesRes.setBounds(1723, 523, 80, 50);
		frmPrincipal.getContentPane().add(lblPTotalesRes);

		JLabel lblPPM = new JLabel("Pulsaciones por minuto:");
		lblPPM.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPPM.setBounds(1475, 584, 198, 50);
		frmPrincipal.getContentPane().add(lblPPM);

		lblPPMRes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPPMRes.setBounds(1723, 584, 80, 50);
		frmPrincipal.getContentPane().add(lblPPMRes);

		JLabel lblErrores = new JLabel("Errores:");
		lblErrores.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblErrores.setBounds(1475, 645, 198, 50);
		frmPrincipal.getContentPane().add(lblErrores);

		lblErroresRes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblErroresRes.setBounds(1723, 645, 80, 50);
		frmPrincipal.getContentPane().add(lblErroresRes);

		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTiempo.setBounds(1475, 706, 198, 50);
		frmPrincipal.getContentPane().add(lblTiempo);

		lblContador.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblContador.setBounds(1723, 695, 80, 69);
		frmPrincipal.getContentPane().add(lblContador);

		JLabel lblContenedor = new JLabel("");
		lblContenedor.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		lblContenedor.setBounds(1445, 494, 464, 454);
		frmPrincipal.getContentPane().add(lblContenedor);

		JLabel lblInfo = new JLabel("Datos no guardados");
		lblInfo.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblInfo.setBounds(1445, 74, 413, 37);
		frmPrincipal.getContentPane().add(lblInfo);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setBounds(1445, 13, 132, 50);
		frmPrincipal.getContentPane().add(btnSalir);

		JButton btnVolver = new JButton("VOLVER");
		btnVolver.setBounds(1587, 13, 132, 50);
		frmPrincipal.getContentPane().add(btnVolver);

		JButton btnGuardar = new JButton("GUARDAR");
		btnGuardar.setBounds(1729, 11, 132, 50);
		frmPrincipal.getContentPane().add(btnGuardar);

		// Evento botones
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int respuesta = JOptionPane.showConfirmDialog(frmPrincipal,
						"Realmente quieres salir? \nNOTA: PERDERS TODO EL PROGRESO", "CERRAR EL PROGRAMA",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0)
					System.exit(0);
			}
		});

		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cnt=0;
				MenuEleccion ME = new MenuEleccion(Usuario);
				int respuesta = JOptionPane.showConfirmDialog(frmPrincipal,
						"Realmente quieres volver? \nNOTA: PERDERS TODO EL PROGRESO SI NO LO HAS GUARDADO", "CERRAR SESI�N",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0) {
					ME.getFrmEleccion().setVisible(true);
					frmPrincipal.dispose();
				}

			}
		});

		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				GuardarEstadisticas();
				lblInfo.setText("Datos guardados correctamente");
				btnGuardar.setEnabled(false);
			}
		});

		// Evento Teclado
		textAreaLectura.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_Q) {
					if (contadorletras == 0)
						Resaltar('Q', lblQ);
					else
						Resaltar('q', lblQ);
				}

				if (e.getKeyCode() == KeyEvent.VK_W) {
					if (contadorletras == 0)
						Resaltar('W', lblW);
					else
						Resaltar('w', lblW);
				}

				if (e.getKeyCode() == KeyEvent.VK_E) {
					if (contadorletras == 0)
						Resaltar('E', lblE);
					else
						Resaltar('e', lblE);
				}

				if (e.getKeyCode() == KeyEvent.VK_R) {
					if (contadorletras == 0)
						Resaltar('R', lblR);
					else
						Resaltar('r', lblR);
				}

				if (e.getKeyCode() == KeyEvent.VK_T) {
					if (contadorletras == 0)
						Resaltar('T', lblT);
					else
						Resaltar('t', lblT);
				}

				if (e.getKeyCode() == KeyEvent.VK_R) {
					if (contadorletras == 0)
						Resaltar('R', lblR);
					else
						Resaltar('r', lblR);
				}

				if (e.getKeyCode() == KeyEvent.VK_Y) {
					if (contadorletras == 0)
						Resaltar('Y', lblY);
					else
						Resaltar('y', lblY);
				}

				if (e.getKeyCode() == KeyEvent.VK_U) {
					if (contadorletras == 0)
						Resaltar('U', lblU);
					else
						Resaltar('u', lblU);
				}

				if (e.getKeyCode() == KeyEvent.VK_I) {
					if (contadorletras == 0)
						Resaltar('I', lblI);
					else
						Resaltar('i', lblI);
				}

				if (e.getKeyCode() == KeyEvent.VK_O) {
					if (contadorletras == 0)
						Resaltar('O', lblO);
					else
						Resaltar('o', lblO);
				}

				if (e.getKeyCode() == KeyEvent.VK_P) {
					if (contadorletras == 0)
						Resaltar('P', lblP);
					else
						Resaltar('p', lblP);
				}

				if (e.getKeyCode() == KeyEvent.VK_A) {
					if (contadorletras == 0)
						Resaltar('A', lblA);
					else
						Resaltar('a', lblA);
				}

				if (e.getKeyCode() == KeyEvent.VK_S) {
					if (contadorletras == 0)
						Resaltar('S', lblS);
					else
						Resaltar('s', lblS);
				}

				if (e.getKeyCode() == KeyEvent.VK_D) {
					if (contadorletras == 0)
						Resaltar('D', lblD);
					else
						Resaltar('d', lblD);
				}

				if (e.getKeyCode() == KeyEvent.VK_F) {
					if (contadorletras == 0)
						Resaltar('F', lblF);
					else
						Resaltar('f', lblF);
				}

				if (e.getKeyCode() == KeyEvent.VK_G) {
					if (contadorletras == 0)
						Resaltar('G', lblG);
					else
						Resaltar('g', lblG);
				}

				if (e.getKeyCode() == KeyEvent.VK_H) {
					if (contadorletras == 0)
						Resaltar('H', lblH);
					else
						Resaltar('h', lblH);
				}

				if (e.getKeyCode() == KeyEvent.VK_J) {
					if (contadorletras == 0)
						Resaltar('J', lblJ);
					else
						Resaltar('j', lblJ);
				}

				if (e.getKeyCode() == KeyEvent.VK_K) {
					if (contadorletras == 0)
						Resaltar('K', lblK);
					else
						Resaltar('k', lblK);
				}

				if (e.getKeyCode() == KeyEvent.VK_L) {
					if (contadorletras == 0)
						Resaltar('L', lblL);
					else
						Resaltar('l', lblL);
				}

				if (e.getKeyCode() == KeyEvent.VK_Z) {
					if (contadorletras == 0)
						Resaltar('Z', lblZ);
					else
						Resaltar('z', lblZ);
				}

				if (e.getKeyCode() == KeyEvent.VK_X) {
					if (contadorletras == 0)
						Resaltar('X', lblX);
					else
						Resaltar('X', lblX);
				}

				if (e.getKeyCode() == KeyEvent.VK_C) {
					if (contadorletras == 0)
						Resaltar('C', lblC);
					else
						Resaltar('c', lblC);
				}

				if (e.getKeyCode() == KeyEvent.VK_V) {
					if (contadorletras == 0)
						Resaltar('V', lblV);
					else
						Resaltar('v', lblV);
				}

				if (e.getKeyCode() == KeyEvent.VK_B) {
					if (contadorletras == 0)
						Resaltar('B', lblB);
					else
						Resaltar('b', lblB);
				}

				if (e.getKeyCode() == KeyEvent.VK_N) {
					if (contadorletras == 0)
						Resaltar('N', lblN);
					else
						Resaltar('n', lblN);
				}

				if (e.getKeyCode() == KeyEvent.VK_M) {
					if (contadorletras == 0)
						Resaltar('M', lblM);
					else
						Resaltar('m', lblM);
				}
				if (e.getKeyCode() == KeyEvent.VK_COMMA){
					if (contadorletras == 0)
						Resaltar(',', label_coma);
					else
						Resaltar(',', label_coma);
				}
				if (e.getKeyCode() == KeyEvent.VK_PERIOD) {
					if (contadorletras == 0)
						Resaltar('.', label_punto);
					else
						Resaltar('.', label_punto);
				}

				if (e.getKeyCode() == KeyEvent.VK_1)
					Resaltar('1', lbl1);

				if (e.getKeyCode() == KeyEvent.VK_2)
					Resaltar('2', lbl2);

				if (e.getKeyCode() == KeyEvent.VK_3)
					Resaltar('3', lbl3);

				if (e.getKeyCode() == KeyEvent.VK_4)
					Resaltar('4', lbl4);

				if (e.getKeyCode() == KeyEvent.VK_4)
					Resaltar('4', lbl4);

				if (e.getKeyCode() == KeyEvent.VK_5)
					Resaltar('5', lbl5);

				if (e.getKeyCode() == KeyEvent.VK_6)
					Resaltar('6', lbl6);

				if (e.getKeyCode() == KeyEvent.VK_7)
					Resaltar('7', lbl7);

				if (e.getKeyCode() == KeyEvent.VK_8)
					Resaltar('8', lbl8);

				if (e.getKeyCode() == KeyEvent.VK_9)
					Resaltar('9', lbl9);

				if (e.getKeyCode() == KeyEvent.VK_0)
					Resaltar('0', lbl0);

				if (e.getKeyCode() == KeyEvent.VK_SPACE)
					Resaltar(' ', lblSPACE);
				if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
					e.consume();
			}

		});

	}
}
