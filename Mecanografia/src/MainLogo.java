import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.JButton;

public class MainLogo {

	//******************************************************
	//Poner revision de ficheros en la barra de carga con mensaje de error si nos los encunetra //
	//Revisar el color de las letras y teclas para que se cambie correctamente, junto a las estadisticas
	//poner boton en menuEleccin para poder ver las estadisticas
	//retocar  los botones de salir volver y guardar en principal

	private JFrame frmMecano;
	private JFrame cargaLogo;

	//imagenes
	Image img = new ImageIcon("IMG\\IMGCarga.jpg").getImage();
	File usuarios = new File("FILES\\usuarios.txt");
	File leccion = new File("FILES\\Texto.txt");


	//Temporizador jProgres
	Timer time;
	int i;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainLogo window = new MainLogo();
					window.frmMecano.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainLogo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecano = new JFrame();
		frmMecano.setTitle("Mecano.exe");
		frmMecano.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		frmMecano.setBounds(500, 150, 853, 633);
		frmMecano.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecano.getContentPane().setLayout(null);

		JProgressBar carga = new JProgressBar();
		carga.setStringPainted(true);
		carga.setBounds(31, 555, 146, 14);
		frmMecano.getContentPane().add(carga);

		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.setVisible(false);
		btnContinuar.setBounds(203, 534, 126, 38);
		btnContinuar.setFocusPainted(false);
		btnContinuar.setFocusTraversalKeysEnabled(false);
		frmMecano.getContentPane().add(btnContinuar);

		JLabel imagenCarga = new JLabel("");
		imagenCarga.setIcon(new ImageIcon(img.getScaledInstance(837, 594, Image.SCALE_SMOOTH)));
		imagenCarga.setBounds(0, 11, 837, 594);
		frmMecano.getContentPane().add(imagenCarga);

		//Eventos

		time = new Timer (30, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				i++;
				carga.setValue(i + 1);
				if(i==100) {
					time.stop();
					if(usuarios.exists() && leccion.exists()) 
					{
						btnContinuar.setVisible(true);
						JOptionPane.showMessageDialog(null,
								"Carga correcta",
								"Datos confirmados",
								JOptionPane.INFORMATION_MESSAGE);
					}
					else 
					{
						JOptionPane.showMessageDialog(null,	"Ficheros de configuracion no encontrados.", "Error", JOptionPane.ERROR_MESSAGE);
						System.exit(1);
					}


				}
			}		
		});
		time.start();


		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login login = new Login();
				login.getFrmLogin().setVisible(true);
				frmMecano.dispose();
			}
		});

	}
}
