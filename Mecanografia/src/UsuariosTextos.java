import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UsuariosTextos {

	public String Contenido(String archivo, String opcion) throws FileNotFoundException, IOException {
		String cadena = "",
				texto = "",
				practica = "";
		BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(archivo), "utf-8"));
		while((cadena = b.readLine())!=null) {
			System.out.println(cadena); 
			texto += cadena;
		}
		b.close();
		practica = leccion(texto, opcion);

		return practica;
	}
	public String leccion(String texto, String opcion) {

		String buscar = "";

		if(opcion == "Leccion 1") {
			opcion = "Leccion 2";
			buscar = "Leccion 1";
		}
		else if (opcion == "Leccion 2") {
			opcion = "Leccion 3";
			buscar = "Leccion 2";
		}

		int inicio = texto.indexOf(buscar);
		int fin = texto.indexOf(opcion, inicio + 9);

		String leccion = texto.substring(inicio + 9, fin);

		return leccion;
	}
	//Usuario y contraseņa
	public boolean busca(String usuario, String passwd, String fichero, boolean login)  throws FileNotFoundException
	{
		String Comprueba = "";
		try {
			BufferedReader leer = new BufferedReader(new FileReader (fichero));
			while((Comprueba = leer.readLine())!=null) {
				if(Comprueba.contentEquals(usuario + " " + passwd)) {
					login = true;
				}else if (!login) {
					login = false;
				}
			}
			leer.close();
		}
		catch(IOException e) {
			System.out.println("No se puede hacer nada");
		}
		return login;

	}

}
